import React, { Component } from 'react'
import Button from "./Button";
import './todolist.less';

let counter = 0

export default class TodoList extends Component {
  constructor(props) {
    super(props);
    console.log('constructor');
  }

  componentDidMount() {
    console.log('componentDidMount');
  }

  componentDidUpdate() {
    console.log('componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  handleAdd = () => {
    const { list, onChange } = this.props
    onChange(list.concat([{ content: 'List Title' + counter++ }]))
  }

  render() {
    console.log('render')
    const { props: { list }, handleAdd } = this

    return (
      <div className='TodoList'>
        <Button onClick={handleAdd} type="secondary" style={{ marginBottom: '20px' }}>Add</Button>
        <ul>
          {
            list.map(({ content }, index) => (<li key={index}>{content}</li>))
          }
        </ul>
      </div>
    )
  }
}

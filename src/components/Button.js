import React, { Component } from 'react'
import './button.less'

export default class Button extends Component {
  render() {
    const { props } = this
    const { children, type = 'primary' } = props

    const styles = ['button', type].join(' ')

    return (
      <button {...props} className={styles}>{children}</button>
    )
  }
}
